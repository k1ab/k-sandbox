
Compile:

```
kompile file-with-definition.k
```

Run:

```
krun program.extention
```

or

```
krun -cPGM='inline_code(goes_here())'
```

Parse:

First compile the K definition and parse the programm based on the definition:
```
kast --output kore program.extention
```