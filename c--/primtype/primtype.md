```k
requires "primtype-syntax.md"

module PRIMTYPE
    imports PRIMTYPE-SYNTAX
    imports BOOL
    imports INT

    rule #isSigned(int8_t) => true
    rule #isSigned(int16_t) => true
    rule #isSigned(int32_t) => true
    rule #isSigned(int64_t) => true
    rule #isSigned(uint8_t) => false
    rule #isSigned(uint16_t) => false
    rule #isSigned(uint32_t) => false
    rule #isSigned(uint64_t) => false
    rule #isUnsigned(I) => notBool #isSigned(I)

    rule #bitwidth(int8_t) => 8
    rule #bitwidth(int16_t) => 16
    rule #bitwidth(int32_t) => 32
    rule #bitwidth(int64_t) => 64
    rule #bitwidth(uint8_t) => 8
    rule #bitwidth(uint16_t) => 16
    rule #bitwidth(uint32_t) => 32
    rule #bitwidth(uint64_t) => 64

    rule #signedForBitwidth(Bits) => int8_t requires Bits >=Int 0 andBool Bits <=Int 8
    rule #signedForBitwidth(Bits) => int16_t requires Bits >=Int 9 andBool Bits <=Int 16
    rule #signedForBitwidth(Bits) => int32_t requires Bits >=Int 17 andBool Bits <=Int 32
    rule #signedForBitwidth(Bits) => int64_t requires Bits >=Int 33 andBool Bits <=Int 64

    rule #supertype(I1, I2) => #signedForBitwidth(maxInt(#bitwidth(I1), #bitwidth(I2)))
      requires #isSigned(I1) andBool #isSigned(I2)
    rule #supertype(I1, I2) => #unsignedForBitwidth(maxInt(#bitwidth(I1), #bitwidth(I2)))
      requires #isUnsigned(I1) andBool #isUnsigned(I2)

endmodule

```
