```k
module PRIMTYPE-SYNTAX
    imports BOOL-SYNTAX
    imports INT-SYNTAX
    syntax Integral ::= "int8_t" [token]
                      | "int16_t" [token]
                      | "int32_t" [token]
                      | "int64_t" [token]
                      | "uint8_t" [token]
                      | "uint16_t" [token]
                      | "uint32_t" [token]
                      | "uint64_t" [token]

    syntax Bool ::= #isSigned(Integral) [function]
                  | #isUnsigned(Integral) [function]

    syntax Int ::= #bitwidth(Integral) [function]

    syntax Integral ::= #supertype(Integral, Integral) [function]
                      | #signedForBitwidth(Int) [function]
                      | #unsignedForBitwidth(Int) [function]
endmodule

```