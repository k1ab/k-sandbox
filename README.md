# K framework sandbox

Tutorials and experiments with K framework.

## Getting started

Clone the repository:

```
git clone https://gitlab.com/k1ab/k-sandbox.git
cd k-sandbox
```

Create a folder with your name and put any files related to exploring k-framework.

The folder should contain README.md file where you keep the current state of your
explorations and code files that you build.

## Resources

- [K-framework tutorial](https://kframework.org/k-distribution/k-tutorial/).
- Language definition [tutorial](https://kframework.org/k-distribution/pl-tutorial/) based on K-framework.
- Grigore Rosu courses on PL semantics [cs422](https://fsl.cs.illinois.edu/teaching/2021/cs422/) and [cs522](https://fsl.cs.illinois.edu/teaching/2021/cs522/).


## K-framework

- [GitHub repo](https://github.com/runtimeverification/k)

***

# Editing this README

Add any useful general info on K-framework to this README file.

## License
MIT license

## Project status
Active learning :-)